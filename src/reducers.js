/***************************************
 ** O-rizon development
 ** Created by Bastien Cecere
 ** 20/07/2017 - 18:27
 ** reducers.js
 ** 2017 - All rights reserved
 ***************************************/

import { combineReducers } from 'redux'
import getRandomColor from 'randomcolor'


const initialState = {
	carres: [
		{
			color: getRandomColor()
		},
		{
			color: getRandomColor()
		},
		{
			color: getRandomColor()
		},
		{
			color: getRandomColor()
		}
	]
}

const AppReducer = (state = initialState, action) => {
	switch (action.type) {
		default:
			return state;
	}
}

const reducers = combineReducers({
	AppReducer
})

export default reducers